describe('Error 1', function () {

    beforeEach(function(){
        cy.visit('http://localhost:3000');
        cy.get('input[name="username"]').type('admin');
        cy.get('input[name="password"]').type('admin');
        click('Sign in');
    });
    it('Mostrar error cuando no hay preguntas en una encuesta', function () {
        
        
        click('Create a new form');
        cy.get('input[name="title"]').type('Encuesta Prueba');
        click('Create form');
        click('Encuesta Prueba');
        click('Share');
        click('View Live');
        cy.contains('{{myform.startPage.introTitle}}');
        cy.go('back');
        click('Delete Form');
        cy.get('.modal-footer').find('input[type="text"]').type('Encuesta Prueba');
        click('I understand the consequences, delete this form.');


    });

    it('No mostrar la flecha del dropdown', function () {
        
        var isValid =  isValidURL('http://filamentgroup.com/files/select-arrow.png');
        expect(isValid).to.be.false;
    });

    function click(name) {
        cy.contains(name).click();
    }
    function isValidURL(url) {
        var isValid = false;
    
        Cypress.$.ajax({
          url: url,
          type: "get",
          async: false,
          success: function(data) {
            isValid = true;
          },
          error: function(){
            isValid = false;
          }
        });
    
        return isValid;
    }
});