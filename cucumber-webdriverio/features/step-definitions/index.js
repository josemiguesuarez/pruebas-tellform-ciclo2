var {
    defineSupportCode
} = require('cucumber');
var {
    expect
} = require('chai');

defineSupportCode(({
    Given,
    When,
    Then
}) => {
    Given('I go to TellForm home screen', () => {
        browser.url('/');
        if (browser.isVisible('a=Signout')) {
            browser.element('a=Signout').click();
        }
    });

    When(/^I fill (.*) and (.*) to sing in$/, (email, password) => {
        var cajaLogIn = browser.element('.signin');
        var mailInput = cajaLogIn.element('input[name="username"]');
        write(mailInput, email);

        var passwordInput = cajaLogIn.element('input[name="password"]');
        write(passwordInput, password);
    });

    When('I try to login', () => {
        var cajaLogIn = browser.element('.signin');
        cajaLogIn.element('button=Sign in').click();
        browser.pause(500);
    });

    When('I go to my profile', () => {
        console.log("browser.element('span=MENU_BTN').value kko", browser.isVisible('span=MENU_BTN'))
        if(browser.isVisible('span=MENU_BTN')){
            click('span=MENU_BTN');
        }
        browser.waitForVisible('=My Settings', 10000);
        browser.element('=My Settings').click();
        browser.waitForVisible('a=Edit Profile', 5000);
        browser.element('a=Edit Profile').click();
    });

    Then(/^I expect to see (.*) as my name$/, (name) => {
        var firstName = browser.element('input[name="firstName"]').getValue();
        expect(firstName).to.include(name);
    });

    Then(/^I create a survey called (.*)$/, (surveyname) => {
        click('.fa-plus');
        write(browser.element('input[name="title"]'), surveyname);
        click('button=Create form');
    });

    Then(/^I add a short text question with this statement (.*)$/, (statement) => {
        click('.fa-pencil-square-o'); //Short text
        write(browser.element('input[name="title"]'), statement);
        click('button=Save');
    });

    Then (/^I go to survey page to fill it$/, ()=>{
        click('.btn-secondary.view-form-btn');
    });

    Then(/^I expect to see the question I have created with this statement (.*)$/, (statement) => {
        browser.pause(1000);
        var h3 = browser.element("form").element('h3').getText();
        expect(h3).to.include(statement);
    });

    Then(/^I should be able to delte the survey with the name (.*)$/, (statement) => {
        browser.url('/');
        click('h4=' +  statement);
        click('.fa-trash-o');
        write(browser.element('input[data-ng-model="deleteConfirm"]'), statement);
        click('button=I understand the consequences, delete this form.');
    });

    When(/^I answer (.*) to (.*)$/, (answer, satetment) => {
        browser.pause(1000);
        var questionsWrappers = browser.elements('form > div').value;
        console.log("questionsWrappers", questionsWrappers);
        questionsWrappers.find(function(questionWrapper){
            console.log("questionWrapper);", questionWrapper.ELEMENT);
            var h3 = browser.elementIdElement(questionWrapper.ELEMENT,'h3');
            if(h3.value){
                if( h3.getText().includes(satetment)){
                    console.log(".log(questionWrapper);", h3.getText());
                    write(browser.elementIdElement(questionWrapper.ELEMENT,'input'), answer);
                    return true;
                }
            }
            return false;
        });
        browser.pause(200);
        click('button=OK');
        browser.pause(200);
        click('.Button');
        browser.pause(1000);
    });
    When(/^I expect to see (.*) in the dashboard of (.*)$/, (answer, surveyname) => {
        browser.url('/');
        click('h4=' +  surveyname);
        click('a=Analyze');
        browser.pause(400);
        var el = browser.element('td='+ answer);
        expect(el.getText()).to.include(answer);
    });

    When(/^I fill the form with (.*), (.*), (.*), (.*), (.*) and (.*)$/, (name, lastname, email, password, university, deparment) => {
        console.log("Searching for caja sign up");
        var cajaSignUp = browser.element('.cajaSignUp');
        console.log(" caja sign up", cajaSignUp);

        write(cajaSignUp.element('input[name="nombre"]'), name);
        write(cajaSignUp.element('input[name="apellido"]'), lastname);
        write(cajaSignUp.element('input[name="correo"]'), email);
        write(cajaSignUp.element('input[name="password"]'), password);
        cajaSignUp.element('select[name="idUniversidad"]').selectByVisibleText(university);
        browser.pause(500);
        cajaSignUp.element('select[name="idDepartamento"]').selectByVisibleText(deparment);
    });
    When(/^I (accept|not) terms and conditions$/, (accept) => {
        if (accept === "accept") {
            var cajaSignUp = browser.element('.cajaSignUp');
            cajaSignUp.element('input[name="acepta"]').click();
        }
    });

    When('I try to sign up', () => {
        var cajaSignUp = browser.element('.cajaSignUp');
        cajaSignUp.element('button=Registrarse').click();
    });
    


});

function write(element, text) {
    element.click();
    element.clearElement();
    element.keys(text);
}

function click(selector){
    browser.waitForVisible(selector, 5000);
    browser.element(selector).click();
}