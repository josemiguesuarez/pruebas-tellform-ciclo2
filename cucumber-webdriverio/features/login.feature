Feature: Login into TellForm
    As an user I want to authenticate myself within TellForm website

Scenario Outline: User can login

  Given I go to TellForm home screen
    When I fill <email> and <password> to sing in
    And I try to login
    And I go to my profile
    Then I expect to see <name> as my name

    Examples:
      | email   | password   | name             |
      | admin   | admin      | Admin            |