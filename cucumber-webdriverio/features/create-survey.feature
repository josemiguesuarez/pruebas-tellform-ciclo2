Feature: Create survey
    As an user I want to create a survey 

Scenario Outline: User can create a survey

  Given I go to TellForm home screen
    When I fill admin and admin to sing in
    And I try to login
    And I create a survey called <surveyname>
    And I add a short text question with this statement <statement>
    And I go to survey page to fill it 
    Then I expect to see the question I have created with this statement <statement>
    And I should be able to delte the survey with the name <surveyname> 

    Examples:
      | surveyname   | statement   | name             |
      | SurveyToCreate   | How are you? | Admin            |