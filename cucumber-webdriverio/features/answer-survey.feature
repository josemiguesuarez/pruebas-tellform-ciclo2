Feature: Answer survey
    As an user I want to be able to answer a survey 

Scenario Outline: User can create a survey

  Given I go to TellForm home screen
    When I fill admin and admin to sing in
    And I try to login
    And I create a survey called <surveyname>
    And I add a short text question with this statement <statement>
    And I go to survey page to fill it 
    And I answer <answer> to <statement>
    Then I expect to see <answer> in the dashboard of <surveyname>
    And I should be able to delte the survey with the name <surveyname> 

    Examples:
      | surveyname   | statement   | answer             |
      | SurveyAnswer   | How are you? | Fine            |