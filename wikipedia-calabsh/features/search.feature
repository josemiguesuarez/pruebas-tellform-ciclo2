Feature: Search

  Scenario Outline: As user I want to search some words
    When I press "Search Wikipedia"
    And I enter text "<word>" into field with id "search_src_text"
    And I press "<result>"
    Then I should see "<type>"
    And I press "Navigate up"
    And I should see "Search Wikipedia"

    Examples:
      | word        | result      | type               |
      | calabash    | Calabash    | Species of plant   |
      | cucumber    | Cucumber    | Species of plant   |
      | lamborghini | Lamborghini | Italian car manufacturer   |

  Scenario Outline: As user I want to see my search history
    When I press "History"
    Then I should see "<result>"
    And I press "Explore"
    And I should see "Search Wikipedia"

    Examples:
      | result      |
      | Calabash    |
      | Cucumber    |
      | Lamborghini |
