Feature: Create and add articles to my reading list

  Scenario: As user I want to create a reading list
    When I press "Search Wikipedia"
    And I enter text "calabash" into field with id "search_src_text"
    And I press "Calabash"
    And I press "Add this article to a reading list"
    And I press "onboarding_button"
    And I clear input field with id "reading_list_title"
    And I enter text "Species of plants" into field with id "reading_list_title"
    And I press "OK"
    And I press "Navigate up"
    And I press "My lists"
    Then I should see "Species of plants"
    And I press "Species of plants"
    And I should see "Calabash"
    And I press "reading_list_detail_back_button"
    And I press "Nearby"
    And I press "Explore"
    And I should see "Search Wikipedia"

  Scenario Outline: As user I want to add articles to my reading list
    When I press "Search Wikipedia"
    And I enter text "<word>" into field with id "search_src_text"
    And I press "<result>"
    And I press "Add this article to a reading list"
    And I press "Species of plants"
    And I press "Navigate up"
    And I press "My lists"
    And I press "Species of plants"
    Then I should see "<result>"
    And I press "reading_list_detail_back_button"
    And I press "Explore"
    And I should see "Search Wikipedia"

    Examples:
      | word        | result      |
      | cucumber    | Cucumber    |
      | garlic      | Garlic      |
      | barley      | Barley      |
