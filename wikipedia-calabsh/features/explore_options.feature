Feature: Explore

  Scenario: As user I want to see my initial page
    Given I press "Explore"
    Then I should see "In the news"
    And I should see "Featured article"
    And I wait for 5 seconds
    And I drag from 50:70 to 50:20 moving with 10 steps
    And I should see "Trending"
    And I drag from 50:20 to 50:70 moving with 10 steps
    And I should see "Search Wikipedia"

